{
  open Lexing
  open Parser

  exception Lexing_error of string

  let keywords = Hashtbl.create 97
  let level = ref 1
  let () = List.iter (fun (s,t) -> Hashtbl.add keywords s t)
  	  ["else", ELSE; "false", FALSE; "fn", FN; "if", IF; "let", LET;
	   "mut", MUT; "return", RETURN; "struct", STRUCT; "true", TRUE;
	   "while", WHILE;]
}

let digit = ['0'-'9']
let integer = digit+
let alpha = ['a'-'z' 'A'-'Z']
let ident = alpha (alpha | digit | '_')*
let chr = [^ '\\' '\"' ] | "\\\\" | "\\n" | "\\\""
let white = [' ' '\t']

rule token = parse
	 | white+ { token lexbuf }
	 | "/*" { level := 1; comment lexbuf; token lexbuf }
	 | "//" [^ '\n']* "\n" 
	 | '\n'  { Lexing.new_line lexbuf; token lexbuf }
	 | integer as n { INTEGER (int_of_string n) }
	 | '&' { REF } 
	 | '+' { PLUS }
	 | "->" { ARROW }
	 | '-' { MINUS }
	 | '*' { TIMES }
	 | '/' { DIV }
	 | '%' { MOD }
	 | ',' { COMMA }
	 | '.' { DOT }
	 | ':' { COLON }
	 | ';' { SEMICOLON }
	 | '(' { LPAR }
	 | ')' { RPAR }
	 | '{' { LCURLY }
	 | '}' { RCURLY }
	 | '[' { LBRACE }
	 | ']' { RBRACE }
	 | '=' { SEQUAL }
	 | '!' { BANG }
	 | "==" { DEQUAL }
	 | "!=" { DIFF }
	 | "&&" { AND }
	 | "||" { OR }
	 | '<' { LT }
	 | '>' { GT }
	 | "<=" { LEQ }
	 | ">=" { GEQ }
	 | ident as s  { match Hashtbl.find_opt keywords s with
			  		  | None -> IDENT s
					  | Some k -> k }
	 | '"' (chr* as s) '"' { STR (Scanf.unescaped s) }
	 | eof { EOF }
  	 | _ { raise (Lexing_error "Unexpected character") }

and comment = parse
	 | "/*" { incr level; comment lexbuf }
	 | "*/" { decr level; if !level > 0 then comment lexbuf }
	 | _    { comment lexbuf }
	 | eof  { raise (Lexing_error "Comment not closed") }