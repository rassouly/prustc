# Rapport projet compilation

## Différences avec le sujet

   - J'ai modifié la relation $\leq$ sur les types en ajoutant la règle
   suivante:

	   $$t \leq t' \Rightarrow \mathrm{Vec}\langle t \rangle \leq
	   	   		   			   \mathrm{Vec}\langle t' \rangle $$
   J'ai utilisé cette nouvelle règle pour permettre au typeur
   d'accepter des expressions comme `vec![& a, &mut b]` (qui est typé
   $\mathrm{Vec}\langle \&t \rangle$ si `a` et `b` ont tous deux le type $t$).

   - Je n'appelle jamais la fonction "free". Cela a pour effet que
     aucun tableau n'est désalloué pendant l'éxecution du programme.
     Cela n'affecte pas le résultat de l'éxecution du programme mais
     rend plus facile les débordements de mémoire.


## Lexeur

Le lexeur est très simple et il est très proche de ce que l'on a vu en
cours avec notemment une gestion des commentaires imbriqués par une
variable `level` explicite.

Comme dans le cours, le lexeur utilise une table de hachage pour les mots-clés.


## Parseur

Le parseur est dans les fichiers parser.mly pour le code menhir et
`ast.mli` pour la définition des types de l'abre de syntaxe abstraite.

L'arbre de syntaxe abstraite est décorée par des informations sur la
position.

Le parseur reprend quasiment littéralement le sujet à quelques
exceptions près:

- J'ai ajouté une règle _\<else\>_ pour pouvoir simplifier la règle _\<if\>_.

- J'ai listé les opérateurs binaires et unaire dans des règles
 _\<binop\>_ et _\<unop\>_ avec le mot-clé `inline` pour garantir que
 la priorité des opérateurs soit respectée.

- Pour la règle _\<bloc\>_, j'ai du réimplémenter une sorte de
  `separated_list` pour les instructions à l'intérieur du bloc car la
  dernière ligne du bloc peut être une expression.

## Typeur

Pour le typeur, j'ai fait le choix de ne pas réutiliser les types
définis dans ast.mli pour l'arbre de syntaxe abstraite décoré des
types que je renvoie. Cet arbre garde également les informations de
position dans le fichier qui seront utilisées par les borrow checker
pour renvoyer des erreurs.

Le point d'entrée du fichier est la fonction `type_ast` qui type
successivement toutes les déclarations. Le typage se fait en une passe
ce qui force le programmeur à ordonner ses déclarations dans un ordre
tel qu'une déclaration ne fasse jamais référence à une déclaration
plus bas dans le fichier. Ce choix est similaire à celui fait par
OCaml mais n'est pas celui qui a été fait par rustc.

Pour décrire le type d'une expression, j'utilise le type `rich_type`
qui un triplet. Le premier élement permet de savoir si l'expression
est une lvalue ou une rvalue, le deuxième encode la mutabilité de
l'expression et le troisième est le type à proprement parler.

Je ne décore que les expressions de ce type riche. Les autres noeuds
de l'arbre de syntaxe qui nécessitent une information de typage sont
les instructions `if` (qui sont en Rust des expressions) car toutes
les autres instructions ont un type Unit. Typer les instructions `if`
nécessite également de typer les blocs. Pour encoder le type des blocs
et des `if`, je n'utilise que le type algébrique `typ` car tous les
blocs et tous les `if` sont des rvalues non mutables.

Les règles de déreférencement automatiques sont explicitées à cette
étape.

Les tableaux vides sont traités un peu spécialement. J'ai ajouté un
constructeur `EmptyVec` à mon type `typ` pour représenter le type d'un
tableau vide. Tout comme rustc, je refuse de typer l'expression
```
	let a = vec![];
```

Le reste de l'implémention suit exactement le sujet (à l'exception des
tableaux hétérogènes comme mentionné au-début).


## Production de code

La production de code se fait dans le fichier `compile.ml`. Aucune
optimisation n'est faite et la pile est utilisée pour stocker toutes
les valeurs intermédiaires.

J'ai fait le choix de représenter les variables de type `Vec`$\langle
\tau \rangle$ un peu différemment: au lieu de les représenter comme
une structure

```
struct Vec { void* contents; size_t length; };
```

, j'ai choisi de les représenter par un simple pointeur vers une zone
de la mémoire allouée avec `malloc` de taille $ns+8$ avec $n$ le
nombre d'entrées du tableau et $s$ la taille en mémoire de chaque
entrée. Ainsi, la taille du tableau est stockée dans les 8 premiers
octets de ce bloc et le reste du tableau suit.

Dans le code assembleur généré les fonctions sont préfixés d'un
underscore afin d'éviter les conflits avec la librairie standard C qui
est liée par le compilateur.

Il y a plusieurs variables globales dans ce fichier:

   - `string_id` et `lab_id` sont des entiers qui représentent
     respectivement le nombre de chaînes et de labels qui ont été
     utilisés. Ils sont utilisés pour générer des noms de chaîne et de
     label uniques.

   - `ret_offset` contient l'offset de l'adresse de retour par rapport
     au frame pointer.

   - `genv` est une structure dont le type vient du typeur. Elle
     contient la liste de toutes les fonctions et structures ainsi que
     le type de retour de la fonction actuelle.

   - `lenv` contient une map des offsets des variables locales ainsi
     que des champs des structures. Le champ `cur_ofs` contient aussi
     le plus grand offset actuellement utilisé. Ce champ est utilisé
     pour trouver une place dans la pile pour stocker des variables locales.


Pour copier une structure (le seul type de données de plus de 8
octets) j'ai implémenté une fonction `memcpy` qui prend en argument
$n$ un nombre d'octets et qui renvoie le code nécessaire pour copier
$n$ octets de l'adresse pointée par rsi à l'adresse pointée par
rdi. Cette fonction est basée sur l'utilisation de l'instruction `rep
movsb` qui copie rcx octets de rsi à rdi.

# Gestion des erreurs

La gestion des erreurs se trouve dans la fonction `set_err` fichier
`helper.ml`. Cette fonction s'appuie sur une table d'erreurs stockée
dans la variable `error_vec`. Ceci permet d'éviter d'avoir de longues
chaînes de caractères au milieu du typeur et du borrow checker.