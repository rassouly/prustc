all: prustc

prustc:
	ocamlbuild -use-menhir -menhir="menhir -v" -no-links main.native
	cp _build/main.native prustc

.PHONY: clean prustc
clean:
	rm prustc
	rm -rf _build

test-syntax: prustc
	tests/test.sh -1 $(shell pwd)/prustc

test-typing: prustc
	tests/test.sh -2 $(shell pwd)/prustc

test-borrowck: prustc
	tests/test.sh -2b $(shell pwd)/prustc

test-exec: prustc
	tests/test.sh -3 $(shell pwd)/prustc

tests: prustc
	tests/test.sh -all $(shell pwd)/prustc
