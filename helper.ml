open Lexing
open Printf

(* Common enums used in parsing, typing and borrow checking *)

type mut =
  | Mut
  | NonMut

type left =
  | Left
  | NonLeft

type binop = | Dequal | Diff | Lt | Leq | Gt | Geq
             | Add | Sub | Mul | Div | Mod | And | Or | Sequal

type unop = | Min | Not | Deref | Ref | RefMut

type loc = position * position


exception TypeError of string


let error_vec = [|
  (* E00 *)
  ["Unimplemented"];

  (* Typing errors *)

  (* E01 *)
  ["Your file must contain a function named main that takes no arguments \
    and doesn't return anything."];

  (* E02 *)
  [""; "Function body is expected to have type "; " but has type "; "."];

  (* E03 *)
  [""; "Function return type cannot be a borrowed type."];

  (* E04 *)
  [""; "Function "; " previously defined\n"; "here."];

  (* E05 *)
  [""; "Structure field cannot have a borrowed type."];

  (* E06 *)
  [""; "Structure fields must have distinct names."];

  (* E07 *)
  [""; "Structure "; " previously defined\n"; "here."];

  (* E08 *)
  [""; ""; " is not a polymorphic struct"];

  (* E09 Not Used *)
  [""; "Unknown type "; "."];

  (* E10 *)
  [""; "The arithmetic and comparaison operators expect 2 integer operands but \
        here the operands have type "; " and "; "."];

  (* E11 *)
  [""; "Unknown variable "; "."];

  (* E12 *)
  [""; "The logic operators && and || expect 2 boolean operands but here \
        the operands have type "; " and "; "."];

  (* E13 *)
  [""; "The type "; " cannot be automatically converted to " ; "."];

  (* E14 *)
  [""; "A non mutable variable cannot be assigned to."];

  (* E15 *)
  [""; "This expression is not a left value and thus cannot be assigned to."];

  (* E16 *)
  [""; "This expression is not a left value and thus cannot be taken \
        as reference."];

  (* E17 *)
  [""; "This expression is not mutable and thus cannot be taken \
        as mutable reference."];

  (* E18 *)
  [""; "Incorrect number of fields in the declaration of a variable of \
        type "; "."];

  (* E19 *)
  [""; "Cannot dereference a non reference type."];

  (* E20 *)
  [""; "Vec index must be an integer."];

  (* E21 *)
  [""; "Non Vec expressions cannot be indexed. This expression has type "; "."];

  (* E22 *)
  [""; "Arguments must have different names."];

  (* E23 *)
  [""; "No field "; " in structure "; "."];

  (* E24 *)
  [""; ""; " is not a struct type. Its fields cannot be accessed."];

  (* E25 *)
  [""; "Unknown structure "; "."];

  (* E26 *)
  [""; "If and while statements expect a boolean expression but this exression \
        has type "; "."];

  (* E27 *)
  [""; "If and else blocks must have the same type. If type : ";
   ", else type : "; "."];

  (* E28 *)
  [""; "While blocks must have a () type. This block has type "; "."];

  (* E29 *)
  [""; "The equality operators expect the same type for both operands.\
        Got types "; " and "; "."];

  (* E30 *)
  [""; "The field "; " expects a type "; " but got a type "; "."];

  (* E31 *)
  [""; "Field "; " must be defined when instantiating a structure of type "; "."];

  (* E32 *)
  [""; "Incorrect return type. Expected "; " but found "; "."];

  (* E33 *)
  [""; "Len operator can only be applied to a Vec but this expression has type"; "."];

  (* E34 *)
  [""; "Cannot deduce the type of "; "."];

  (* E35 *)
  [""; "An argument of type "; " was expected but an expression of type ";
   " was found."];

  (* E36 *)
  [""; "A structure can only reference itsels with a Vec."];

  (* E37 *)
  [""; "Unknown function "; "."];

  (* E38 *)
  [""; "Mismatched types in Vec declaration."];

  (* E39 *)
  [""; "Function "; " expects " ;" arguments but only "; " were given."];

  (* E40 *)
  [""; "Logic operator ! expects a boolean operand but this expression \
        has type "; "."];

  (* Borrow checker errors *)

  (* E41 *)
  [""; "Variable "; " was already moved.\n"; "here."];

  (* E42 *)
  [""; "Variable "; " cannot be used because there is mutable reference \
                     pointing to it.\n"; "here."];

  (* E43 *)
  [""; "Variable "; " cannot be taken as a mutable reference because there \
                     is still another reference pointing to it.\n";
  "here."];

  (* E44 *)
  [""; "Variable "; " cannot be assigned to because there is still \
                     another reference pointing to it."];

  (* E45 *)
  [""; "Variable "; " is dropped before all of its references are dropped."];

  (* E46 *)
  [""; "Variable "; " cannot be mutably borrowed or moved while it is still \
                     borrowed\n"; "here"];
|]

(* Uses the standard format for error position to enable the use
   of emacs next-error function *)
let localisation pos =
    let f = pos.pos_fname in
    let l = pos.pos_lnum in
    let c = pos.pos_cnum - pos.pos_bol + 1 in
    sprintf "File \"%s\", line %d, characters %d-%d:\n" f l (c-1) c

(* Used in the typer to indicate an entire region.
   Assumes that the region is localized in a single file. *)
let local2 (pos1, pos2) =
    let f = pos2.pos_fname in
    let l1 = pos1.pos_lnum in
    let c1 = pos1.pos_cnum - pos1.pos_bol in
    let l2 = pos2.pos_lnum in
    let c2 = pos2.pos_cnum - pos2.pos_bol in
    if l1 != l2 then
      sprintf "File \"%s\", lines %d-%d, characters %d-%d:\n" f l1 l2 c1 c2
    else
      sprintf "File \"%s\", line %d, characters %d-%d:\n" f l1 c1 c2

(* Used in the typer to make error handling more concise. The first
   argument i is an integer that refers to an error in the
   type_error_vec array. The second argument is a list of strings. The
   list of strings in error_vec is zipped with the second argument and
   everything is then sent in a TypeError function. *)
let set_err i args =
  let error_string = sprintf "[E%02d] " i in
  let s = List.fold_left2 (fun acc a b -> acc ^a ^ b)
      (List.hd error_vec.(i))
      args
      (List.tl error_vec.(i)) in
  raise (TypeError (s ^ error_string ^ "\n"))


