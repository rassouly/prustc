%{
(* Some common types used in multiple stages of the compilation are
in the Helper module *)
open Helper
open Ast
%}

%token ELSE FALSE FN IF LET MUT RETURN STRUCT TRUE WHILE
%token BANG
%token <string> IDENT
%token <int> INTEGER
%token <string> STR
%token PLUS MINUS TIMES DIV MOD COMMA DOT COLON SEMICOLON SEQUAL DEQUAL
%token LT GT LEQ GEQ AND OR DIFF
%token LCURLY RCURLY LPAR RPAR LBRACE RBRACE ARROW REF
%token EOF


%right SEQUAL
%left OR
%left AND
%nonassoc DEQUAL DIFF LT LEQ GT GEQ
%left PLUS MINUS
%left TIMES DIV MOD
%nonassoc uni
%nonassoc LBRACE
%nonassoc DOT

%start file

%type <Ast.file> file

%%

file: d = decl* EOF { d };

typed_var: s = IDENT COLON t = type_annot { (s,t) }

decl:
	| STRUCT s = IDENT LCURLY f=separated_list(COMMA, typed_var) RCURLY
	  	 { {decl_desc=DeclStruct (s, f); decl_loc=$startpos,$endpos} }
	| FN s = IDENT LPAR a = separated_list(COMMA, arg) RPAR
	  ARROW t = type_annot b = block
	  	 { {decl_desc=DeclFun (s, a, Some t, b); decl_loc=$startpos,$endpos} }
	| FN s = IDENT LPAR a = separated_list(COMMA, arg) RPAR b = block
	  	 { {decl_desc=DeclFun (s, a, None, b); decl_loc=$startpos,$endpos} }
;	

type_annot:
	| s = IDENT
	  	{ {type_annot_desc=TypeName s; type_annot_loc=$startpos,$endpos} }
	| s = IDENT LT t = type_annot GT 
	  	{ {type_annot_desc=Template (s,t); type_annot_loc=$startpos,$endpos} }
	| REF t = type_annot
	  	{ {type_annot_desc=TypeRef (NonMut,t);
		   type_annot_loc=$startpos,$endpos} }
	| REF MUT t = type_annot
	  	{ {type_annot_desc=TypeRef (Mut,t);
		   type_annot_loc=$startpos,$endpos} }
;

arg:
	| t = typed_var
	  	{ {arg_desc=(NonMut, fst t, snd t); arg_loc=$startpos,$endpos} }
	| MUT t = typed_var
	  	{ {arg_desc=(Mut, fst t, snd t); arg_loc=$startpos,$endpos} }
;

block_instr:
	| { {block_desc=([], None); block_loc=$startpos,$endpos} }
	| e = expr
	  	{ {block_desc=([], Some e); block_loc=$startpos,$endpos} }
	| i = instr b = block_instr
	  	{ let l, e = b.block_desc in
		  {block_desc=(i::l, e); block_loc=$startpos,$endpos} }
;

block: LCURLY b = block_instr RCURLY { b };

struct_field: f = IDENT COLON e = expr { (f,e) }; 

instr:
	| SEMICOLON { {instr_desc=Empty; instr_loc=$startpos,$endpos} }
	| e = expr SEMICOLON { {instr_desc=Expr e; instr_loc=$startpos,$endpos} }
	| LET v1 = IDENT SEQUAL v2 = expr SEMICOLON
	  	  { {instr_desc=Decl (NonMut, v1, v2); instr_loc=$startpos,$endpos} }
	| LET MUT v1 = IDENT SEQUAL v2 = expr SEMICOLON
	  	  { {instr_desc=Decl (Mut, v1, v2); instr_loc=$startpos,$endpos} }
	| LET v = IDENT SEQUAL s = IDENT
	  LCURLY a = separated_list(COMMA, struct_field) RCURLY SEMICOLON
	  	  { {instr_desc=StructInst (NonMut, v, s, a);
		     instr_loc=$startpos,$endpos} }
	| LET MUT v = IDENT SEQUAL s = IDENT   
	  LCURLY a = separated_list(COMMA, struct_field) RCURLY SEMICOLON
	  	  { {instr_desc=StructInst (Mut, v, s, a);
			 instr_loc=$startpos,$endpos} }
	| WHILE e = expr b = block
	  	  { {instr_desc=While (e, b); instr_loc=$startpos,$endpos} }
	| RETURN e=expr? SEMICOLON
	  	  { {instr_desc=Return e; instr_loc=$startpos,$endpos} }
	| i = sif { {instr_desc=If i; instr_loc=$startpos,$endpos} }
;

sif: IF e = expr b = block s=selse?
	 { {if_desc=e, b, s; if_loc=$startpos,$endpos} };

selse:
	| ELSE b = block { {else_desc=ElseBlock b; else_loc=$startpos,$endpos} }
	| ELSE s = sif { {else_desc=ElseIf s; else_loc=$startpos,$endpos} }
;

expr:
	| n = INTEGER { {expr_desc=Const n; expr_loc=$startpos, $endpos} }
	| TRUE { {expr_desc=Bconst true; expr_loc=$startpos, $endpos} }
	| FALSE { {expr_desc=Bconst false; expr_loc=$startpos, $endpos} }
	| s = IDENT { {expr_desc=Var s; expr_loc=$startpos, $endpos} }
	| b = block { {expr_desc=Block b; expr_loc=$startpos, $endpos} }
	| s1 = expr o = binop s2 = expr
	  	 { {expr_desc=Binop (o, s1, s2); expr_loc=$startpos, $endpos} }
	| o = unop s = expr %prec uni
	  	{ {expr_desc=Unop (o, s); expr_loc=$startpos, $endpos} }
	| LPAR s = expr RPAR { s }
	| e = expr DOT i = IDENT
	  	{ {expr_desc=Dot (e, i); expr_loc=$startpos, $endpos} }
	| e1 = expr LBRACE e2 = expr RBRACE
	  	{ {expr_desc=At (e1, e2); expr_loc=$startpos, $endpos} }
	| f = IDENT LPAR a = separated_list(COMMA, expr) RPAR
	  	{ {expr_desc=FunApp (f, a); expr_loc=$startpos, $endpos} }
	| i = IDENT BANG LPAR s = STR RPAR
	  	{ if i = "print" then {expr_desc=Print s; expr_loc=$startpos, $endpos}
	  	  else raise Exit }
	| i = IDENT BANG LBRACE e = separated_list(COMMA, expr) RBRACE
	  	{ if i = "vec" then {expr_desc=Vec e; expr_loc=$startpos, $endpos}
	      else raise Exit }
	| e = expr DOT i = IDENT LPAR RPAR {
	  if i = "len" then 
	  	 { expr_desc=(Len e);
	  	   expr_loc=$startpos, $endpos}
	  else raise Exit }
;

%inline binop:
	| PLUS { Add }
	| MINUS { Sub }
	| TIMES { Mul }
	| DIV { Div }
	| MOD { Mod }
	| AND { And }
	| OR { Or }
	| SEQUAL { Sequal }
	| DEQUAL { Dequal }
	| DIFF { Diff }
	| LT { Lt }
	| GT { Gt }
	| LEQ { Leq }
	| GEQ { Geq }
;

%inline unop:
	| MINUS { Min }
	| BANG { Not }
	| TIMES { Deref }
	| REF { Ref }
	| REF MUT { RefMut}
;