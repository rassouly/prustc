open Format
open Helper

let parse_only = ref false
let type_only = ref false
let no_asm = ref false

let ifile = ref ""
let ofile = ref ""

let set_file f s = f := s

let options =
    ["--parse-only", Arg.Set parse_only,
     "  Stop after lexing and parsing";
     "--type-only", Arg.Set type_only,
     "  Stop after lexing, parsing and typing";
     "--no-asm", Arg.Set no_asm,
     "  Stop after lexing, parsing, typing and borrow checking"]

let usage = "usage: prustc [options] file.rs"

let () =
    Arg.parse options (set_file ifile) usage;

    if !ifile="" then begin eprintf "No input file\n@?"; exit 1 end;

    if not (Filename.check_suffix !ifile ".rs") then begin
        eprintf "The input file must have a .rs extension\n@?";
        Arg.usage options usage;
        exit 1
    end;
    ofile := (Filename.chop_suffix !ifile ".rs") ^ ".s";

    let f = open_in !ifile in
    let buf = Lexing.from_channel f in
    (* Set the correct filename *)
    buf.Lexing.lex_curr_p <- {
      Lexing.pos_fname = !ifile;
      pos_lnum = 1;
      pos_bol = 0;
      pos_cnum = 0;
    };
    try
        let t = Parser.file Lexer.token buf in
        close_in f;

        if !parse_only then exit 0;

        let f, env = Typer.type_ast t in
        if !type_only then exit 0;

        Borrowck.check f;
        if !no_asm then exit 0;

        let p = Compile.compile_ast env f in
        X86_64.print_in_file ~file:!ofile p;

    with
      | Lexer.Lexing_error c ->
	      eprintf "%s" (localisation (Lexing.lexeme_start_p buf));
	      eprintf "Lexical error: %s\n@?" c;
	      exit 1

      | Parser.Error ->
	      eprintf "%s" (localisation (Lexing.lexeme_start_p buf));
	      eprintf "Syntax error\n@?";
	      exit 1

      (* This is an error thrown in the parser when a macro other than
         vec! or print! is parsed.
         A custom exception type cannot be used because menhir refuses
         to export it in the Parser module *)
      | Exit ->
          eprintf "%s" (localisation (Lexing.lexeme_start_p buf));
	      eprintf "Unknown macro. Did you mean vec! or print!?\n@?";
	      exit 1

      | Helper.TypeError s ->
          eprintf "%s\n@?" s;
          exit 1;
