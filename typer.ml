open Helper
open Printf

type return =
    | Ret
    | NoRet

type typ =
    | Unit
    | I32
    | Bool
    | Struct of string
    | Tvec of typ
    | EmptyVec
    | Tref of mut * typ

type rich_type = left*mut*typ

type expr = {
  expr_desc: expr_desc;
  expr_type: rich_type;
  expr_loc: loc;
}

and expr_desc =
    | Const of int
    | Bconst of bool
    | Var of string
    | Binop of binop * expr * expr
    | Unop of unop * expr
    | Dot of expr * string
    | Len of expr
    | At of expr * expr
    | FunApp of string * expr list
    | Vec of expr list
    | Print of string
    | Block of block

and selse =
    | ElseBlock of block
    | ElseIf of sif

and sif = expr * block * selse option

and instr =
    | Empty
    | Expr of expr
    | Decl of mut * string * expr
    | StructInst of mut * string * string * (string * expr) list
    | While of expr * block
    | Return of expr option
    | If of sif * typ

and block = {
  block_desc: instr list * expr option;
  block_type: typ;
  block_loc: loc;
}

type arg = string * rich_type

type decl =
    | DeclStruct of string * (string * typ) list
    | DeclFun of string * (arg list) * typ * block

module SMap = Map.Make(String)

type fn_type = arg list * typ


(* Global (i.e. program wide) typing environment *)
type genv = {
  mutable fns: (fn_type * loc) SMap.t;
  mutable structs: ((string * typ) list * loc) SMap.t;
  mutable ret_type: typ;
}


(* Take a variable name v, a structure name and the map between structure names
   and field names and return the list of all possible children of v
   ([v.a; v.b; v.c; ...]).
   This function will list those children recursively ([v.a.a; v.a.b; ...])..
*)
let rec list_children vname sname strs =
  List.fold_left (fun acc (f, t) ->
      (match t with
        | Struct s -> list_children (vname^"."^f) s strs
        | _ -> [])
      @((vname^"."^f) :: acc)
    ) [vname] (SMap.find sname strs)


let type_of_expr e =
  let _, _, t = e.expr_type in t

let rec print_type = function
  | Unit -> "()"
  | I32 -> "i32"
  | Bool -> "bool"
  | Struct s -> s
  | Tvec t -> sprintf "Vec<%s>" (print_type t)
  | EmptyVec -> "Vec<T>"
  | Tref (m, t) -> sprintf "&%s %s"
                     (if m = Mut then "mut" else "")
                     (print_type t)

(* Compute t1 <= t2 *)
let rec is_acceptable t1 t2 =
  match t1, t2 with
    | t1', t2' when t1'=t2' -> true
    | Tvec _, EmptyVec -> true
    | Tvec t1', Tvec t2' -> is_acceptable t1' t2'
    | Tref (Mut, t1'), Tref (_, t2') when t1'=t2' -> true
    | _ -> false

(* Compute li such that, for all j, lj <= li *)
let common_type l =
  let result = ref None in
  List.iter (fun a ->
      if List.for_all (fun b -> is_acceptable b a) l then result := Some a) l;
  !result

(* All the type_* functions return a pair. The first element of the
   pair is the AST decorated with the types.

   The second element indicates whether or not evaluating this
   expression/block/instruction will necessarilly return from the
   function. This is needed to correctly type functions whose body
   doesn't end with an expression. We don't keep this information for
   the next step.

   When they encounter an error, these functions use the set_err
   function defined in the helper.ml file. This function in turn
   throws an exception which is caught in the main.ml. *)
let rec type_expr expr env lenv =
  let e = expr.Ast.expr_desc in
  let loc = expr.Ast.expr_loc in
  match e with
    | Ast.Const n -> { expr_desc = Const n;
                       expr_type = NonLeft, NonMut, I32;
                       expr_loc = loc }, false
    | Ast.Bconst b -> { expr_desc = Bconst b;
                        expr_type = NonLeft, NonMut, Bool;
                        expr_loc = loc }, false
    | Ast.Var s -> begin
        match SMap.find_opt s lenv with
          | None -> set_err 11 [local2 loc; s]
          | Some (_, m, t) -> { expr_desc = Var s;
                                expr_type = Left, m, t;
                                expr_loc = loc }, false
      end
    | Ast.Binop (b, e1, e2) -> begin
        let e1', r1 = type_expr e1 env lenv in
        let e2', r2 = type_expr e2 env lenv in
        let l1, m1, t1 = e1'.expr_type in
        let t2 = type_of_expr e2' in
        let r = r1 || r2 in
        match b with
          | Dequal | Diff ->
            if t1 = t2 then
              { expr_desc = Binop(b, e1', e2');
                expr_type = NonLeft, NonMut, Bool;
                expr_loc = loc }, r
            else
              set_err 29 [local2 loc; print_type t1; print_type t2]
          | Gt | Lt | Geq | Leq ->
            if t1 = I32 && t2 = I32 then
              { expr_desc = Binop(b, e1', e2');
                expr_type = NonLeft, NonMut, Bool;
                expr_loc = loc }, r
            else
              set_err 10 [local2 loc; print_type t1; print_type t2]
          | Add | Sub | Mul | Div | Mod  ->
            if t1 = I32 && t2 = I32 then
              { expr_desc = Binop(b, e1', e2');
                expr_type = NonLeft, NonMut, I32;
                expr_loc = loc  }, r
            else
              set_err 10 [local2 loc; print_type t1; print_type t2]
          | And | Or -> begin
              if t1 = Bool && t2 = Bool then
                { expr_desc = Binop(b, e1', e2');
                  expr_type = NonLeft,NonMut,Bool;
                  expr_loc = loc }, r1
              else
                set_err 12 [local2 loc; print_type t1; print_type t2]
            end
          | Sequal -> begin
              if l1 = Left then begin
                if m1 = Mut then begin
                  if is_acceptable t2 t1 then
                    { expr_desc = Binop(b, e1', e2');
                      expr_type = NonLeft, NonMut, Unit;
                      expr_loc = loc }, r
                  else
                    set_err 13 [local2 loc; print_type t2; print_type t1]
                end
                else
                  set_err 14 [local2 e1.Ast.expr_loc]
              end
              else
                set_err 15 [local2 e1.Ast.expr_loc]
            end
      end
    | Ast.Unop (u, e) -> begin
        let e', r = type_expr e env lenv in
        let l, m, t = e'.expr_type in
        match u with
          | Min -> if t = I32 then
              { expr_desc = Unop(u, e');
                expr_type = NonLeft, NonMut, I32;
                expr_loc = loc }, r
            else set_err 10 [local2 loc]
          | Not -> if t = Bool then
              { expr_desc = Unop(u, e');
                expr_type = NonLeft, NonMut, Bool;
                expr_loc = loc }, r
            else set_err 40 [local2 loc; print_type t]
          | Ref -> if l = Left then
              { expr_desc = Unop(u, e');
                expr_type = NonLeft, NonMut, Tref (NonMut, t);
                expr_loc = loc }, r
            else set_err 16 [local2 loc]
          | RefMut ->
            if l = Left then begin
              if m = Mut then
                { expr_desc = Unop(u, e');
                  expr_type = NonLeft, NonMut, Tref (Mut, t);
                  expr_loc = loc }, r
              else set_err 17 [local2 loc]
            end
            else set_err 16 [local2 loc]
          | Deref -> begin
              match t with
                | Tref (m, t') ->
                  { expr_desc = Unop(u, e'); expr_type = Left, m, t';
                    expr_loc = loc }, r
                | _ -> set_err 19 [local2 loc]
            end
      end
    | Ast.At (e1, e2) -> begin
        let e1', r1 = type_expr e1 env lenv in
        let e2', r2 = type_expr e2 env lenv in
        let l1, m1, t1 = e1'.expr_type in
        let t2 = type_of_expr e2' in
        let r = r1 || r2 in
        match t1 with
          | Tvec tr ->
            if t2 = I32 then
              { expr_desc = At (e1', e2');
                expr_type = (Left, m1, tr);
                expr_loc = loc }, r
            else
              set_err 20 [local2 e2.Ast.expr_loc]
          (* Handle one of the auto deref rules *)
          | Tref (m, Tvec tr) ->
            if t2 = I32 then
              let deref_e1' =
                { expr_desc = Unop (Deref, e1');
                  expr_type = (l1, m1, Tvec tr);
                  expr_loc = e1'.expr_loc } in
              { expr_desc = At (deref_e1', e2');
                expr_type = (Left, m, tr);
                expr_loc = loc }, r
            else
              set_err 20 [local2 e2.Ast.expr_loc]
          | _ -> set_err 21 [local2 e1.Ast.expr_loc; print_type t1]
      end
    | Ast.Dot (e, f) -> begin
        let e', r = type_expr e env lenv in
        let l, m, t = e'.expr_type in
        match t with
          | Struct s -> begin
              let fields, _ = SMap.find s env.structs in
              match List.find_opt (fun (name, _) -> name = f) fields with
                | None -> set_err 23 [local2 loc; f; s]
                | Some (fname, ftype) ->
                  { expr_desc = Dot (e', f);
                    expr_type = (Left, m, ftype);
                    expr_loc = loc }, r
            end
          (* Handle one of the auto deref rules *)
          | Tref (m', Struct s) -> begin
              let fields, _ = SMap.find s env.structs in
              match List.find_opt (fun (name, _) -> name = f) fields with
                | None -> set_err 23 [local2 loc; f; s]
                | Some (fname, ftype) ->
                  let deref_e' =
                    { expr_desc = Unop (Deref, e');
                      expr_type = (l, m, Struct s);
                      expr_loc = e'.expr_loc } in
                  { expr_desc = Dot (deref_e', f);
                    expr_type = (Left, m', ftype);
                    expr_loc = loc }, r
            end
          | _ -> set_err 24 [local2 e.Ast.expr_loc; print_type t]
      end
    | Ast.Print s -> { expr_desc = Print s;
                       expr_type = NonLeft, NonMut, Unit;
                       expr_loc = loc }, false
    | Ast.FunApp (fname, args) -> begin
        match SMap.find_opt fname env.fns with
          | None -> set_err 37 [local2 loc; fname]
          | Some ((fargs, ret_type), floc) ->
            if List.length fargs <> List.length args then
              set_err 39 [local2 loc; fname; string_of_int (List.length fargs);
                          string_of_int (List.length args)]
            else begin
              let typed_args, r = List.fold_left2 (fun acc a (_, (_, _, ft)) ->
                  let a', r = type_expr a env lenv in
                  let t = type_of_expr a' in
                  if not (is_acceptable t ft) then
                    set_err 35 [local2 a.Ast.expr_loc;
                                print_type ft; print_type t]
                  else (a' :: (fst acc), r || snd acc)
                ) ([], false) args fargs in
              { expr_desc = FunApp (fname, typed_args);
                expr_type = (NonLeft, NonMut, ret_type);
                expr_loc = loc }, r
            end
      end
    | Ast.Len e -> begin
        let e', r = type_expr e env lenv in
        let l', m', t' = e'.expr_type in
        match t' with
          (* Handle empty vecs *)
        | Tvec _ | EmptyVec ->
          { expr_desc = Len e';
            expr_type = NonLeft, NonMut, I32;
            expr_loc = loc }, r
        (* Handle one of the auto deref rules *)
        | Tref (_, Tvec tr)->
            let deref_e' =
              { expr_desc = Unop (Deref, e');
                expr_type = (l', m', Tvec tr);
                expr_loc = e'.expr_loc } in
            { expr_desc = Len deref_e';
              expr_type = NonLeft, NonMut, I32;
              expr_loc = loc }, r
          | _ -> set_err 33 [local2 loc; print_type t']
      end
    | Ast.Vec el -> begin
        match el with
          | [] ->
            { expr_desc = Vec [];
              expr_type = NonLeft, NonMut, EmptyVec;
              expr_loc = loc }, false
          | _ ->
            let r = ref false in
            let el' = List.map (fun e ->
                let e', r' = type_expr e env lenv in
                r := !r || r'; e') el in
            let tl = List.map type_of_expr el' in
            (* Here, we compute the type of a heterogeneous vec *)
            let t = match common_type tl with
              | None -> set_err 38 [local2 loc]
              | Some x -> x in
            { expr_desc = Vec el';
              expr_type = NonLeft, NonMut, Tvec t;
              expr_loc = loc }, !r
      end
    | Ast.Block b ->
      let b, r = type_block b env lenv in
      { expr_desc = Block b;
        expr_type = NonLeft, NonMut, b.block_type;
        expr_loc = loc }, r


and type_instr instr env lenv =
  let loc = instr.Ast.instr_loc in
  match instr.Ast.instr_desc  with
    | Ast.Empty -> Empty, false
    | Ast.Expr e -> let e, r = type_expr e env lenv in Expr e, r
    | Ast.(If { if_desc = (e, b, eopt); if_loc = loc }) ->
      let e', r = type_expr e env lenv in
      let te = type_of_expr e' in
      let b', ri = type_block b env lenv in
      if te <> Bool then
        set_err 26 [local2 e.Ast.expr_loc; print_type te]
      else begin
        let else_block, else_type, re = match eopt with
          | None -> None, Unit, false
          | Some Ast.({ else_desc = eopt_desc ; else_loc = eopt_loc }) -> begin
              match eopt_desc with
                | Ast.ElseBlock b ->
                  let eb', re = type_block b env lenv in
                  Some (ElseBlock eb'), eb'.block_type, re
                | Ast.ElseIf if_b -> begin
                    match type_instr Ast.({instr_desc= If if_b;
                                           instr_loc=eopt_loc}) env lenv with
                      | If (eb, t), re -> Some (ElseIf eb), t, re
                      | _ -> failwith "Unexpected error"
                  end
            end
        in if else_type <> b'.block_type then
          set_err 27 [local2 loc; print_type b'.block_type; print_type else_type]
        else
          If ((e', b', else_block), else_type), r || (ri && re)
      end
    | Ast.While (condition, b) ->
      let condition, r = type_expr condition env lenv in
      let condition_type = type_of_expr condition in
      let b', _ = type_block b env lenv in
      if condition_type <> Bool then
        set_err 26 [local2 loc; print_type condition_type]
      else begin
        if b'.block_type <> Unit then set_err 28 [local2 b.Ast.block_loc;
                                                  print_type b'.block_type]
        else While (condition, b'), r
      end
    | Ast.Return e -> begin
        match e with
          | None -> Return None, true
          | Some e ->
            let e', _ = type_expr e env lenv in
            let t = type_of_expr e' in
            begin match t, env.ret_type with
              | EmptyVec, Tvec _ -> Return (Some e'), true
              | t, t' when t = t' -> Return (Some e'), true
              | _ ->
                set_err 32 [local2 loc; print_type env.ret_type; print_type t]
            end
      end

    (* We should never reach this point because let statements are
       typed within the type_block function *)
    | Ast.Decl _ | Ast.StructInst _ -> assert false


and type_block b env lenv =
  match b.Ast.block_desc with
    | [], None -> { block_desc = [], None;
                    block_type = Unit;
                    block_loc = b.Ast.block_loc }, false
    | [], Some e ->
      let e, r = type_expr e env lenv in
      let t = type_of_expr e in
      { block_desc = [], Some e;
        block_type = t;
        block_loc = b.Ast.block_loc }, r

    | Ast.({ instr_desc = Empty; instr_loc = _ }) :: br, rete ->
      type_block Ast.({ block_desc = (br, rete);
                       block_loc = b.Ast.block_loc })
        env lenv

    | Ast.({ instr_desc = Decl (m, s, e); instr_loc = loc }) :: br, rete ->
      let e', r = type_expr e env lenv in
      let t = type_of_expr e' in
      if t = EmptyVec then
        set_err 34 [local2 loc; s];
      let b', r' = type_block Ast.({ block_desc = (br, rete); block_loc = loc })
          env (SMap.add s (Left, m, t) lenv) in
      let t' = b'.block_type in
      let bd' = b'.block_desc in
      { block_desc = Decl (m, s, e') :: (fst bd'), snd bd';
        block_type = t';
        block_loc = b.Ast.block_loc }, r || r'

    | Ast.({ instr_desc = StructInst (m, v_name, s, f); instr_loc = loc }) :: br,
      rete ->
      let s', _ = match SMap.find_opt s env.structs with
        | None -> set_err 25 [local2 loc; s]
        | Some x -> x
      in
      let f', r = List.fold_left (fun (acc, r') (n, t) ->
          match List.find_opt (fun (a, _) -> a = n) f with
            | None -> set_err 31 [local2 loc; n; s]
            | Some (n', e) -> begin
                let e', r = type_expr e env lenv in
                let _, _, t' = e'.expr_type in
                (* Allow empty vec when a vec is expected *)
                match t, t' with
                  | Tvec _, EmptyVec -> (n', e') :: acc, r || r'
                  | t, t' when t = t' -> (n', e') :: acc, r || r'
                  | t, t' ->
                    set_err 30 [local2 loc; n'; s; print_type t; print_type t']
              end
        ) ([], false) s' in
      if List.length s' <> List.length f then
        set_err 18 [local2 loc; print_type (Struct s)]
      else begin
        let b', r' = type_block Ast.({ block_desc = (br, rete); block_loc = loc })
            env (SMap.add v_name (Left, m, Struct s) lenv) in
        let t' = b'.block_type in
        let bd' = b'.block_desc in
        { block_desc = StructInst (m, v_name, s, f') :: (fst bd'), snd bd';
          block_type = t';
          block_loc = b.Ast.block_loc }, r || r'
      end

    | Ast.({instr_desc = If _; instr_loc = _}) :: [], None as a ->
      let i', r = type_instr (List.hd (fst a)) env lenv in
      let t = match i' with
        | If (_, a) -> a
        | _ -> assert false in
      { block_desc = [i'], None;
        block_type = t;
        block_loc = b.Ast.block_loc }, r

    | i :: br, rete ->
      let i', r = type_instr i env lenv in
      let b', r' = type_block Ast.({ block_desc = (br, rete);
                                    block_loc = b.block_loc })
          env lenv in
      let bd' = b'.block_desc in
      { block_desc = i' :: (fst bd'), snd bd';
        block_type = b'.block_type;
        block_loc = b.Ast.block_loc }, r || r'


let rec type_type_annot t env =
  let open Ast in
  match t.type_annot_desc with
    | TypeName s -> begin
        match s with
          | "i32" -> I32
          | "bool" -> Bool
          | s -> begin match SMap.find_opt s env.structs with
              | None -> set_err 25 [local2 t.type_annot_loc; s]
              | Some _ -> Struct s
            end
      end
    | Template (s, annot) ->
      if s <> "Vec" then
        set_err 8 [local2 t.type_annot_loc; s]
      else Tvec (type_type_annot annot env)
    | TypeRef (m, annot) -> Tref (m, type_type_annot annot env)


let type_decl d env =
  match d.Ast.decl_desc with
    | Ast.DeclStruct (name, fields) -> begin
        if SMap.mem name env.structs then
          let open Ast in
          set_err 7 [local2 d.decl_loc; name;
                     local2 (snd (SMap.find name env.structs))]
        else begin
          env.structs <- SMap.add name ([], d.Ast.decl_loc)
              env.structs;
          let typed_fields = List.fold_left (fun acc (s, t) ->
              if List.exists (fun (a, _) -> a = s) acc then
                set_err 6 [local2 d.Ast.decl_loc]
              else
                match type_type_annot t env with
                  | Tref _ -> set_err 5 [local2 t.Ast.type_annot_loc]
                  | Struct s when s = name ->
                    set_err 36 [local2 t.Ast.type_annot_loc]
                  | _ as t' -> (s, t') :: acc
            ) [] fields in
          env.structs <- SMap.add name (typed_fields, d.Ast.decl_loc)
              env.structs;
          DeclStruct (name, typed_fields)
        end
      end
    | Ast.DeclFun (name, args, ret, bl) -> begin
        if SMap.mem name env.fns then
          set_err 4 [local2 d.Ast.decl_loc; name;
                     local2 (snd (SMap.find name env.fns))]
        else begin
          let ret_type, pos = match ret with
            | None -> Unit, (Lexing.dummy_pos, Lexing.dummy_pos)
            | Some t -> type_type_annot t env, t.Ast.type_annot_loc
          in match ret_type with
            | Tref _ -> set_err 3 [local2 pos]
            | _ -> ();
              let args_type = List.fold_left
                  (fun acc t ->
                     let m, s, annot = t.Ast.arg_desc in
                     if List.exists (fun (a, _) -> a = s) acc then
                       set_err 22 [local2 d.Ast.decl_loc];
                     (s, (Left, m, type_type_annot annot env)) :: acc
                  ) [] args in
              let args_type = List.rev args_type in
              env.fns <- SMap.add name ((args_type, ret_type),
                                        d.Ast.decl_loc) env.fns;
              env.ret_type <- ret_type;

              let bl', r = type_block bl env
                  (List.fold_left (fun acc (n, t) -> SMap.add n t acc)
                     SMap.empty args_type)
              in
              (* Handle empty vecs separately *)
              begin match bl'.block_type, ret_type with
                | EmptyVec, Tvec _ -> DeclFun (name, args_type, ret_type, bl')
                | t, t' when t = t' -> DeclFun (name, args_type, ret_type, bl')
                | Unit, _ when r -> DeclFun (name, args_type, ret_type, bl')
                | _ ->
                  set_err 2 [local2 bl.Ast.block_loc;
                             print_type ret_type;
                             print_type bl'.block_type]
              end
        end
      end


(* Takes the AST coming out the parser, throws an exception if the
   program cannot be typed and otherwise returns a new AST decorated
   with some type information. It also returns the set of all
   functions and structures used in the program (of type genv). *)

let type_ast t =
  let env = {fns = SMap.empty; structs = SMap.empty; ret_type = Unit} in
  let rst = List.fold_left (fun acc d -> (type_decl d env)::acc) [] t in

  if not (SMap.mem "main" env.fns) ||
     fst (SMap.find "main" env.fns) <> ([], Unit) then set_err 1 []
  else rst, env
