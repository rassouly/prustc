open X86_64
open Typer
open Helper
open Printf

type block_out = {
  block_code: X86_64.text;
  block_data: X86_64.data;
}

let string_id = ref 0
let lab_id = ref 0
let ret_offset = ref 0


let genv = {
  structs = SMap.empty;
  fns = SMap.empty;
  ret_type = Unit;
}

type lenv = {
  mutable vset: int SMap.t;
  mutable cur_ofs: int;
  mutable offsets: (int SMap.t) SMap.t; (* list of fields with their
                                           respective offsets for every
                                           structure *)
}

let lenv = {
  vset = SMap.empty;
  cur_ofs = 0;
  offsets = SMap.empty;
}


let rec sizeof = function
  | Bool | I32 | Unit | Tref _ | Tvec _ | EmptyVec -> 8
  | Struct s ->
    let fields, _ = SMap.find s genv.structs in
    List.fold_left (fun acc (_, ftype) -> acc + sizeof ftype ) 0 fields

(* Copies n bytes from the *rsi to *rdi *)
let memcpy n =
  movq (imm n) (reg rcx) ++
  inline "\trep movsb\n"

let rec compile_block b =
  let il, e = b.block_desc in
  let il = match e with
    | None -> il
    | Some e' -> il @ [Expr e']
  in
  let rst = {
    block_code = nop;
    block_data = nop; } in
  List.fold_left (fun acc i ->
      match i with
      | Empty -> acc

      | Expr e ->
        let  c, d = compile_expr e in
        { block_code = acc.block_code ++ c;
          block_data = acc.block_data ++ d; }

      | Decl (_, s, e) ->
        let n = sizeof (type_of_expr e) in
        let ofs = - lenv.cur_ofs - n in
        let c, d = compile_expr e in
        let c = c ++
                if n > 8 then
                  movq (reg rsp) (reg rsi) ++
                  leaq (ind ~ofs rbp) rdi ++
                  memcpy n ++
                  (* Clean up the stack *)
                  addq (imm n) (reg rsp)
                else
                  popq rax ++
                  movq (reg rax) (ind ~ofs rbp)
        in
        lenv.vset <- SMap.add s ofs lenv.vset;
        lenv.cur_ofs <- -ofs ;
        { block_code = acc.block_code ++ c;
          block_data = acc.block_data ++ d }

      | StructInst (_, vname, sname, fields) ->
        let n = sizeof (Struct sname) in
        let ofs = -lenv.cur_ofs - n in
        let offsets = SMap.find sname lenv.offsets in
        lenv.vset <- SMap.add vname ofs lenv.vset;
        lenv.cur_ofs <- -ofs;
        let  c, d = List.fold_left (fun (acc_c, acc_d) (fname, e) ->
            let lofs = SMap.find fname offsets in
            let c, d = compile_expr e in
            let n = sizeof (type_of_expr e) in
            let c = c ++
                    if n > 8 then
                      movq (reg rsp) (reg rsi) ++
                      leaq (ind ~ofs:(ofs + lofs) rbp) rdi ++
                      memcpy n ++
                      (* Clean up the stack *)
                      addq (imm n) (reg rsp)
                    else
                      popq rax ++
                      movq (reg rax) (ind ~ofs:(ofs + lofs) rbp)
            in
            acc_c ++ c, acc_d ++ d
          ) (nop, nop) fields in
        { block_code = acc.block_code ++ c;
          block_data = acc.block_data ++ d; }
      | If ((e, b, selse), t) ->
        let cc, dc = compile_expr e in
        let bc = compile_block b in
        let has_else = ref true in
        let selse = begin
          match selse with
          | None -> has_else := false;
            { block_code = nop;
              block_data = nop; }
          | Some (ElseBlock b) -> compile_block b
          | Some (ElseIf i) ->
            compile_block
              { block_desc = [If (i, t)], None;
                block_type = t;
                block_loc = b.block_loc }
        end in
        let l1 = sprintf "L%d" !lab_id in
        incr lab_id;
        let l2 = sprintf "L%d" !lab_id in
        incr lab_id;
        let block_code = acc.block_code ++ cc ++
                        popq rax ++
                        testq (reg rax) (reg rax) ++
                        je l1 ++
                        bc.block_code ++
                        (if !has_else then jmp l2 else nop) ++
                        label l1 ++
                        selse.block_code ++
                        (if !has_else then label l2 else nop)
        in
        let block_data = acc.block_data ++ dc ++
                        bc.block_data ++
                        selse.block_data in
        { block_code; block_data }

      | Return e_opt -> begin
          match e_opt with
          | None ->
            { acc with block_code = acc.block_code ++
                                   (* Clear rax in case we are returning
                                      from main *)
                                   xorq (reg rax) (reg rax) ++
                                   leave ++ ret}
          | Some e ->
            let t = type_of_expr e in
            let n = sizeof t in
            let c, d = compile_expr e in
            let c = c ++
                    begin
                      if n > 8 then
                        movq (reg rsp) (reg rsi) ++
                        leaq (ind ~ofs:!ret_offset rbp) rdi ++
                        memcpy n
                      else
                        popq rax ++ movq (reg rax) (ind ~ofs:!ret_offset rbp)
                    end
                    ++ leave ++ ret
            in
            { block_code = acc.block_code ++ c;
              block_data = acc.block_data ++ d }

        end

      | While (e, b) ->
        let ce, de = compile_expr e in
        let li = sprintf "L%d" !lab_id in
        incr lab_id;
        let le = sprintf "L%d" !lab_id in
        incr lab_id;
        let b' = compile_block b in
        let block_code =
          acc.block_code ++
          label li ++
          ce ++ popq rax ++
          testq (reg rax) (reg rax) ++
          jz le ++
          b'.block_code ++
          jmp li ++
          label le in
        { block_code; block_data = acc.block_data ++ de ++ b'.block_data; }
    ) rst il


(* Put a pointer to the lvalue e in rax.
   Return code and data. *)
and pointer_from_lvalue e =
  match e.expr_desc with
  | Var x -> leaq (ind ~ofs:(SMap.find x lenv.vset) rbp) rax, nop
  | Dot (e', f) ->
    let sname = match type_of_expr e' with
      | Struct s -> s
      | _ -> assert false
    in
    let ofs = SMap.find f (SMap.find sname lenv.offsets) in
    let c, d = pointer_from_lvalue e' in
    c ++ leaq (ind ~ofs rax) rax, d
  | Unop (Deref, e) -> begin
      (* e has to be a reference. If we find a reference operator right after,
         we simplify the two.*)
      match e.expr_desc with
      | Unop (Ref, e) | Unop (RefMut, e) -> pointer_from_lvalue e
      | _ -> let c, d = pointer_from_lvalue e in
        c ++ movq (ind rax) (reg rax), d
    end
  | At (e1, e2) ->
    let n = match type_of_expr e1 with
      | Tvec t -> sizeof t
      | _ -> assert false in
    let c2, d2 = compile_expr e2 in
    let c1, d1 = pointer_from_lvalue e1 in
    c1 ++ pushq (ind rax) ++
    c2 ++ popq rcx ++ popq rax ++ imulq (imm n) (reg rcx) ++
    leaq (ind ~ofs:8 ~index:rcx rax) rax, d1 ++ d2
  | _ -> assert false (* In Rust, a block can also be an lvalue but not in
                         prust *)


and compile_expr e =
  match e.expr_desc with
  | Print s ->
    let l = sprintf ".S%d" !string_id in
    incr string_id;
    let data = label l ++ string s in
    let code =
      xorq (reg rax) (reg rax) ++
      movq (lab ("$"^l)) (reg rdi) ++
      call "printf" in
    code, data
  (* There is no pushq imm64 so we need to used pushq r64*)
  | Const n -> movq (imm n) (reg rax) ++ pushq (reg rax), nop
  | Bconst b -> pushq (imm (if b then 1 else 0)), nop
  | Var s ->
    let ofs = SMap.find s lenv.vset in
    let n = sizeof (type_of_expr e) in
    (if n = 8 then
       pushq (ind ~ofs rbp)
     else
       subq (imm n) (reg rsp) ++
       leaq (ind ~ofs rbp) rsi ++
       movq (reg rsp) (reg rdi) ++
       memcpy n)
  , nop
  | FunApp (f, args) ->
    let ((_, t), _) = SMap.find f genv.fns in
    let ret_size = sizeof t in
    let arg_size = ref 0 in
    let c, d = List.fold_left (fun (acc_c, acc_d) arg ->
        arg_size := !arg_size + sizeof (type_of_expr arg);
        let c, d = compile_expr arg in
        acc_c ++ c, acc_d ++ d
      ) (nop, nop) (args) in
    (if t <> Unit then subq (imm ret_size) (reg rsp) else nop) ++
    c ++ call ("_" ^ f) ++
    addq (imm !arg_size) (reg rsp),
    d

  | Binop (b, e1, e2) -> begin
      let c1, d1 = compile_expr e1 in
      let c2, d2 = compile_expr e2 in
        match b with
        (* Arithmetic operations *)
        | Add | Sub | Mul | Div | Mod ->
          (* Put e1 in rax and e2 in rbx *)
          c1 ++ c2 ++ popq rbx ++ popq rax ++
          (match b with
           | Add -> addq (reg rbx) (reg rax)
           | Sub -> subq (reg rbx) (reg rax)
           | Mul -> imulq (reg rbx) (reg rax)
           | Div -> cqto ++ idivq (reg rbx)
           | Mod -> cqto ++ idivq (reg rbx) ++
                    movq (reg rdx) (reg rax)
           | _ -> assert false
          ) ++ pushq (reg rax), d1 ++ d2

        (* Comparaison operators *)
        | Lt | Gt | Geq | Leq | Dequal | Diff->
          (* Put e1 in rax and e2 in rbx *)
          c1 ++ c2 ++ popq rbx ++ popq rax ++
          cmpq (reg rbx) (reg rax) ++
          (match b with
           | Lt -> setl
           | Gt -> setg
           | Geq -> setge
           | Leq -> setle
           | Dequal -> sete
           | Diff -> setne
           | _ -> assert false
          ) (reg cl) ++ movzbq (reg cl) rcx ++
          pushq (reg rcx), d1 ++ d2

        (* Lazy logic operators *)
        | And | Or ->
          let l = sprintf "L%d" !lab_id in
          incr lab_id;
          c1 ++ popq rax ++
          testq (reg rax) (reg rax) ++
          (* Only evaluate the second expression if we need to *)
          (if b = And then je else jne) l ++
          c2 ++ popq rbx ++
          (if b = And then testq else orq) (reg rax) (reg rbx) ++
          label l ++
          setne (reg cl) ++
          pushq (reg rcx), d1 ++ d2

        | Sequal ->
          let n = sizeof (type_of_expr e2) in
          (* We need to compile e1 as an lvalue and not as an rvalue *)
          let c1, d1 = pointer_from_lvalue e1 in
          c2 ++ c1 ++
          begin if n > 8 then
              movq (reg rsp) (reg rsi) ++
              movq (reg rax) (reg rdi) ++
              memcpy n ++
              addq (imm n) (reg rsp)
            else
              popq rbx ++
              movq (reg rbx) (ind rax)
          end, d1 ++ d2
    end
  | Unop (u, e') -> begin
      match u with
      | Min ->
        let c, d = compile_expr e' in
        c ++ xorq (reg rax) (reg rax) ++ popq rbx ++
        subq (reg rbx) (reg rax) ++ pushq (reg rax),
        d
      | Not -> let c, d = compile_expr e' in
        c ++ popq rax ++ testq (reg rax) (reg rax) ++
        sete (reg al) ++
        movsbq (reg al) rax ++
        pushq (reg rax),
        d
      | Ref | RefMut ->
        (* Ref can only be applied to an lvalue *)
        let c, d = pointer_from_lvalue e' in
        c ++ pushq (reg rax), d
      | Deref ->
        let c, d = compile_expr e' in
        let n = sizeof (type_of_expr e) in
        c ++ popq rax ++
        (if n = 8 then
           movq (ind rax) (reg rax) ++ pushq (reg rax)
         else
           subq (imm n) (reg rsp) ++
           movq (reg rax) (reg rsi) ++
           movq (reg rsp) (reg rdi) ++
           memcpy n)
      , d
    end
  | Dot (es, fname) ->
    let c, d = compile_expr es in
    let sname = match type_of_expr es with
      | Struct s -> s
      | _ -> assert false (* The expression before a dot operator is
                             necessarily a structure *)
    in
    let lofs = SMap.find fname (SMap.find sname lenv.offsets) in
    let struct_size = sizeof (type_of_expr es) in
    let field_size = sizeof (type_of_expr e) in
    c ++
    (if lofs <> struct_size - field_size then
       leaq (ind ~ofs:(lofs) rsp) rsi ++
       leaq (ind ~ofs:(struct_size - field_size) rsp) rdi ++
       memcpy field_size
     else nop )++
    addq (imm (struct_size - field_size)) (reg rsp), d

  | Vec l ->
    let n = match l with
      | [] -> 0
      | e :: _ -> sizeof (type_of_expr e) in
    let length = List.length l in
    let vec_size = length * n + 8 in (* Take into account the field containing
                                        the length of the array *)
    let ofs = - lenv.cur_ofs - 8 in
    let c = movq (imm vec_size) (reg rdi) ++ call "malloc" ++
            movq (reg rax) (ind ~ofs rbp) ++
            movq (imm length) (ind rax) in
    lenv.cur_ofs <- -ofs;
    let lofs = ref (8 - n) in
    let c, d = List.fold_left (fun (acc_c, acc_d) elem ->
        let c, d = compile_expr elem in
        acc_c ++ c ++
        (if n = 8 then
           popq rax ++
           movq (ind ~ofs rbp) (reg rdx) ++
           movq (reg rax) (ind ~ofs:(lofs := !lofs + n; !lofs) rdx)
         else
           movq (reg rsp) (reg rsi) ++
           movq (ind ~ofs rbp) (reg rdx) ++
           leaq (ind ~ofs:(lofs := !lofs + n; !lofs) rdx) rdi ++
           memcpy n ++
           addq (imm n) (reg rsp)
        )
      , acc_d ++ d
      ) (c, nop) l
    in
    c ++ movq (ind ~ofs rbp) (reg rdx) ++
    pushq (reg rdx), d

  | Len e ->
    let c, d = compile_expr e in
    c ++ popq rax ++ pushq (ind rax), d

  | At (e1, e2) ->
    let n = match type_of_expr e1 with
      | Tvec t -> sizeof t
      | _ -> assert false in
    let c1, d1 = compile_expr e1 in
    let c2, d2 = compile_expr e2 in
    c2 ++ c1 ++ popq rax ++ popq rbx ++
    imulq (imm n) (reg rbx) ++
    pushq (ind ~ofs:8 ~index:rbx rax), d1 ++ d2

  | Block b -> let b' = compile_block b in
    b'.block_code, b'.block_data


let compile_decl = function
  | DeclStruct (sname, fields) ->
    nop, nop
  | DeclFun (fname, args, ret_type, b) ->
    (* We add 16 to take into account the ret
       address that was pushed by the call and the
       saved rbp *)
    ret_offset := 16;
    genv.ret_type <- ret_type;
    let vset = List.fold_left (fun acc a ->
        let s, (_, _, t) = a in
        let new_acc = SMap.add s !ret_offset acc in
        let n = sizeof t in
        ret_offset := !ret_offset + n;
        new_acc
      ) SMap.empty args in
    lenv.vset <- vset;
    lenv.cur_ofs <- 0;
    let b = compile_block b in
    (* We need to mangle the function names a little bit to avoid
       conflict with the libc functions such as printf, malloc,
       calloc... except if this is the main function *)
    label (if fname <> "main" then "_" ^ fname else "main") ++
    pushq (reg rbp) ++
    movq (reg rsp) (reg rbp) ++
    (if lenv.cur_ofs > 0 then subq (imm lenv.cur_ofs) (reg rsp) else nop) ++
    b.block_code ++
    begin match ret_type with
      | Unit ->
        (* We need to set rax to 0 in case we return from main *)
        (if fname = "main" then xorq (reg rax) (reg rax) else nop)
        ++ leave ++ ret
      | t ->
        let n = sizeof t in
        begin
          if n = 8 then
            popq rax ++ movq (reg rax) (ind ~ofs:!ret_offset rbp)
          else
            movq (reg rsp) (reg rsi) ++
            leaq (ind ~ofs:!ret_offset rbp) rdi ++
            memcpy n
        end
        ++ leave ++ ret
    end,
    b.block_data


let compile_ast e t =
  genv.fns <- e.fns;
  genv.structs <- e.structs;
  SMap.iter (fun sname (fields, _) ->
      let offsets = ref SMap.empty in
      let lofs = ref 0 in
      List.iter (fun (fname, ftype) ->
          let n = sizeof ftype in
          offsets := SMap.add fname !lofs !offsets;
          lofs := !lofs + n;
        ) fields;
      lenv.offsets <- SMap.add sname !offsets lenv.offsets;
    ) genv.structs;
  let code, data = List.fold_left (fun (acc_c, acc_d) x ->
      let c, d = compile_decl x in
      (c ++ acc_c, d ++ acc_d)) (nop, nop) t in
  let p = {
    text =
      globl "main" ++
      code;
    data = data } in p
