open Typer
open Helper

type status =
    | Free
    | Moved of loc
    | Borrowed of loc
    | MutBorrowed of loc

type ivar = string*int

let dummy_var = ("_", -1)

module RMap = Map.Make(
struct
  type t = ivar
  let compare (s1, l1) (s2, l2) =
    match Pervasives.compare l1 l2 with
      | 0 -> Pervasives.compare s1 s2
      | c -> c
end)

type resource = {
  mutable refs: ivar list;
  mutable mutref: ivar option;
  mutable status: status;
}

let lrset = ref RMap.empty

let structs = ref SMap.empty

(* It turns out that having a persistant data structure with mutable
   variables can be a mess. We need this helper function to actually copy it *)
let deep_copy lrset =
  RMap.fold (fun v rs acc ->
      RMap.union (fun _ v _ -> Some v) acc
        (RMap.singleton v
           { refs = rs.refs;
             mutref = rs.mutref;
             status = rs.status;
           })
    ) lrset RMap.empty


let update_rs old_rs new_rs =
  old_rs.status <- new_rs.status;
  old_rs.refs <- new_rs.refs;
  old_rs.mutref <- new_rs.mutref



(* Return a subset of lrset containing all the children of (s, lvl) *)
let get_children (s, lvl) =
  RMap.filter (fun (s', lvl') _ -> lvl = lvl'
                                   && String.length s' >= String.length s
                                   && String.sub s' 0 (String.length s) = s)
    !lrset


(* Apply a status change to all children resources
   Raises an exception if it fails *)
let update_lrset (s, lvl) new_rs loc =
  (* Force the update on the target resource *)
  update_rs (RMap.find (s, lvl) !lrset) new_rs;
  RMap.iter (fun (s', lvl) rs ->
      if new_rs.status = Free then update_rs rs new_rs
      else
        match rs.status with
          | Free -> update_rs rs new_rs
          | Borrowed locb -> begin
              match new_rs.status with
                | Borrowed _ -> update_rs rs new_rs
                | _ -> set_err 46 [local2 loc; s; local2 locb]
            end
          | MutBorrowed locb -> set_err 42 [local2 loc; s; local2 locb]
          | Moved locm -> set_err 41 [local2 loc; s; local2 locm]
    ) (RMap.filter (fun (x, _) _ -> x <> s) ((get_children (s, lvl))))


let is_resource = function
  | Tref (Mut, _) | Tvec _ | Struct _ -> true
  | _ -> false



(* Check whether an expression is valid or not. This function is
   always called on rvalues. Lvalues are handled in the assignement
   operator case (find_lvalue function).

   Return the variable name (with the lifetime) if the expression is a
   named resource (i.e. like v.a or &mut b). This is used to know which resources
   must be moved/borrowed.
*)
let rec check_expr e lvl =
  (* Little shortcut to throw an exception when an expression contains
     a moved or a mutably borrowed value*)
  let check_read e =
    match check_expr e lvl with
      | None -> None
      | Some x ->
        let rs = RMap.find x !lrset in
        match rs.status with
          | Moved loc -> set_err 41 [local2 e.expr_loc; fst x; local2 loc]
          | MutBorrowed loc ->
            set_err 42 [local2 e.expr_loc; fst x; local2 loc]
          | _ -> Some x
  in

  match e.expr_desc with
    | Const _ | Bconst _ | Print _ -> None
    | Var v ->
      let (v, rs) = RMap.max_binding
          (RMap.filter (fun (s', _) _ -> s' = v) !lrset)
      in Some v
    | Vec l | FunApp (_, l) ->
      List.iter (fun e ->
          match check_expr e lvl with
            | None -> ()
            | Some x ->
              let rs = RMap.find x !lrset in
              match rs.status with
                | Free ->
                  if is_resource (type_of_expr e) then
                    update_lrset x {rs with status = Moved e.expr_loc} e.expr_loc
                (* If the variable is borrowed, make sure that the
                   variable was borrowed inside the function call, if
                   this is so, release it otherwise raise an exception *)
                | Borrowed loc | MutBorrowed loc ->
                  if rs.mutref <> Some dummy_var &&
                     List.hd (rs.refs) <> dummy_var then
                    set_err 42 [local2 e.expr_loc; fst x; local2 loc]
                  else
                    update_lrset x { status = Free; refs = []; mutref = None }
                      e.expr_loc
                | Moved loc ->
                  set_err 41 [local2 e.expr_loc; fst x; local2 loc]
        ) l;
      None
    | Unop (Ref, e) -> begin
        match check_expr e lvl with
          | None -> None
          | Some x ->
            let rs = RMap.find x !lrset in
            match rs.status with
              | Moved loc -> set_err 41 [local2 e.expr_loc; fst x; local2 loc]
              | MutBorrowed loc ->
                set_err 42 [local2 e.expr_loc; fst x; local2 loc]
              | Borrowed _ | Free ->
                (* We set the borrower to dummy_var. This way, if we are in
                   an assignement instruction, we can replace it later with the
                   proper name of the variable and if this is a temporary burrow,
                   we can release it later.*)
                update_lrset x { rs with refs = dummy_var :: rs.refs;
                                         status = Borrowed e.expr_loc }
                  e.expr_loc;
                Some x
      end
    | Unop (RefMut, e) -> begin
        match check_expr e lvl with
          | None -> None
          | Some x ->
            let rs = RMap.find x !lrset in
            match rs.status with
              | Moved loc -> set_err 41 [local2 e.expr_loc; fst x; local2 loc]
              | MutBorrowed loc ->
                set_err 42 [local2 e.expr_loc; fst x; local2 loc]
              | Borrowed loc ->
                set_err 43 [local2 e.expr_loc; fst x; local2 loc]
              | Free ->
                update_lrset x { rs with mutref = Some dummy_var;
                                         status = MutBorrowed e.expr_loc }
                  e.expr_loc;
                Some x
      end
    | Unop (Deref, e) -> check_read e
    (* Unitary operations (except deref) can copy their operand.
       Like the other functions that take const references, len
       just need to check that its argument has not been moved
       or mutably borrowed before*)
    | Unop (_, e) | Len e -> let _ = check_read e in None
    | Dot(e, f) -> begin
        match check_expr e lvl with
          | None -> None
          | Some (v, lvl) -> Some (v^"."^f, lvl)
      end
    | Binop (Sequal, e1, e2) -> begin
        let rec find_lvalue = function
          | Var v -> v
          | Unop (Deref, e) -> find_lvalue e.expr_desc
          | At (e1, e2) -> find_lvalue e1.expr_desc
          | Dot(e1, s) -> (find_lvalue e1.expr_desc) ^ "." ^ s
          (* We sould never reach this point because the typer made
             sure that e1 is a valid lvalue i.e. one of the above *)
          | _ -> assert false
        in
        let s1 = find_lvalue e1.expr_desc in
        let e2' = check_expr e2 lvl in
        let (v1, rs1) = RMap.max_binding
            (RMap.filter (fun (s', _) _ -> s' = s1) !lrset) in


        (* Make sure that the left operand is not borrowed. It is not
           enough to look at the status field since a moved variable
           can potentially have referenced pointing to it thanks to
           if/else branches *)
        if rs1.refs <> [] then
          set_err 44 [local2 e1.expr_loc; fst v1];

        match e2' with
          | None -> ()
          | Some x ->
            let rs = RMap.find x !lrset in
            begin
              match rs.status with
                | Moved loc -> set_err 41 [local2 e.expr_loc; fst x; local2 loc]
                | Borrowed _ -> begin
                    match rs.refs with
                      | x' :: r when x' = dummy_var ->
                        rs.refs <- v1 :: r;
                        (* Lifetime check *)
                        if snd v1 < snd x then
                          set_err 45 [local2 e.expr_loc; fst x]
                      | _ -> ()
                  end
                | MutBorrowed _ -> begin
                    match rs.mutref with
                      | Some x' when x' = dummy_var ->
                        rs.mutref <- Some v1;
                        (* Lifetime check *)
                        if snd v1 < snd x then
                          set_err 45 [local2 e.expr_loc; fst x]
                      | _ -> ()
                  end
                | Free -> ()
            end
      end; None
    (* Binary operations always operate on i32 and bool variables so we
       just need to check that the variables involved have not been moved
       before *)
    | Binop (_, e1, e2) -> let _ = check_read e1, check_read e2 in None
    | At(e1, e2) -> let _ = check_read e2 in check_read e1
    | Block b -> check_block b (lvl+1); None

and check_instr i lvl =
  match i with
    | Empty -> None
    | Expr e -> begin
        match check_expr e lvl with
          | None -> None
          | Some x ->
            let rs = RMap.find x !lrset in
            (match rs.refs with
              | dummy_var :: r -> rs.refs <- r
              | _ -> ());
            (match rs.mutref with
              | Some dummy_var -> rs.mutref <- None
              | _ -> ());
            None
      end
    | Decl (_, v, e) -> begin
        let lt = ref lvl in
        begin match check_expr e lvl with
          | None -> ()
          | Some x -> begin
              let rs = RMap.find x !lrset in
              let t = type_of_expr e in
              (match rs.status with
                | Borrowed loc ->
                  (match rs.refs with
                    | dummy_var :: r -> rs.refs <- ((v, lvl) :: r)
                    | _ -> if is_resource t then set_err 46
                          [local2 e.expr_loc; v; local2 loc]);
                | MutBorrowed loc ->
                  (match rs.mutref with
                    | Some dummy_var -> rs.mutref <- Some (v, lvl)
                    | _ -> if is_resource t then set_err 46
                          [local2 e.expr_loc; v; local2 loc]);
                | Moved loc ->
                  set_err 41 [local2 e.expr_loc; fst x; local2 loc]
                | Free ->
                  if is_resource t then
                    update_lrset x { rs with status = Moved e.expr_loc }
                      e.expr_loc;
                  lt := snd x;
              )
            end;
        end;

        begin
          match type_of_expr e with
            | Struct sname | Tref (_, Struct sname) ->
              List.iter (fun v' ->
                  let new_rs =
                    { refs = [];
                      mutref = None;
                      status = Free
                    } in
                  lrset := RMap.add (v', !lt) new_rs !lrset;
                ) (list_children v sname !structs);
            | _ ->
              let new_rs =
                { refs = [];
                  mutref = None;
                  status = Free
                } in
              lrset := RMap.add (v, !lt) new_rs !lrset;
        end;
        Some (v, !lt)
      end

    | StructInst (_, v, sname, l) -> begin
        List.iter
          (fun (s, e) ->
             match check_expr e lvl with
               | None -> ()
               | Some x -> let rs = RMap.find x !lrset in
                 (match rs.refs with
                   | dummy_var :: r -> rs.refs <- (v, lvl) :: r
                   | _ -> ());
                 (match rs.mutref with
                   | Some dummy_var -> rs.mutref <- Some (v, lvl)
                   | _ -> ());
                 (match rs.status with
                   | Borrowed _ | MutBorrowed _ -> ()
                   | Moved loc ->
                     set_err 41 [local2 e.expr_loc; fst x; local2 loc]
                   | Free ->
                     if is_resource (type_of_expr e) then
                       update_lrset x { rs with status = Moved e.expr_loc }
                         e.expr_loc;)
          ) l;
        List.iter (fun v' ->
            let new_rs =
              { refs = [];
                mutref = None;
                status = Free
              } in
            lrset := RMap.add (v', lvl) new_rs !lrset;
          ) (list_children v sname !structs);
        Some (v, lvl)
      end
    | If ((e, b, else_b), _) -> begin
        let _ = check_instr (Expr e) lvl in
        (* We need to keep track of which variables were free before
           checking the if block because we'll need to merge the ownership
           changes of the if and else block afterwards *)
        let old_lrset = deep_copy !lrset in
        check_block b (lvl+1);
        (match else_b with
          | None -> ()
          | Some (ElseBlock b) -> lrset := old_lrset; check_block b (lvl+1)
          | Some (ElseIf s) ->
            lrset := old_lrset;
            let _ = check_instr (If (s, Unit)) (lvl+1) in () );
        (* Merge lrset and old lrset with the priority MutBorrowed >
           Moved > Borrowed.
           This causes some problem because a moved variable can be assigned
           to whereas a borrowed one cannot. Hence we need to check the refs field
           and the status field in Sequal
        *)
        lrset := RMap.union (fun _ rs1 rs2 ->
            match rs1.status, rs2.status with
              | Free, Free | Moved _, Free
              | Borrowed _, Free | MutBorrowed _, Free | Moved _, Moved _ ->
                Some rs1
              | Free, Moved _ | Free, Borrowed _ | Free, MutBorrowed _ ->
                Some rs2
              | MutBorrowed loc, Borrowed _ | Borrowed loc, MutBorrowed _
              | MutBorrowed loc, MutBorrowed _ | Moved _, MutBorrowed loc
              | MutBorrowed loc, Moved _ ->
                Some { status = MutBorrowed loc;
                       mutref = if rs1.mutref <> None then rs1.mutref
                         else rs2.mutref;
                       refs =  rs1.refs @ rs2.refs }
              | Moved locm, Borrowed locb | Borrowed locb, Moved locm ->
                Some { status = Moved locm;
                       mutref = None;
                       refs = rs1.refs @ rs2.refs }
              | Borrowed loc, Borrowed _ ->
                Some { status = MutBorrowed loc;
                       mutref = None;
                       refs = rs1.refs @ rs2.refs }
          ) !lrset old_lrset;
        None
      end
    (* For return, we just need to check whether the variable has been
       moved before or not *)
    | Return e -> begin
        match e with
          | None -> None
          | Some e -> begin
              match check_expr e lvl with
                | None -> None
                | Some x -> begin
                    match (RMap.find x !lrset).status with
                      | Moved loc ->
                        set_err 41 [local2 e.expr_loc; fst x; local2 loc]
                      | _ -> None
                  end
            end
      end

    | While (e, b) ->
      let _ = check_instr (Expr e) lvl in
      check_block b (lvl+1);
      None


and check_block b lvl =
  let rec aux lvl = function
    | [] -> ()
    | i :: r ->
      let n = check_instr i lvl in
      let decl = match n with
        | Some _ -> true
        | None -> false in

      (* Release all temporary burrows *)
      RMap.iter (fun _ rs ->
          (match rs.refs with
            | x :: r  when x = dummy_var ->
              rs.refs <- r;
            | _ -> ());
          (match rs.mutref with
            | Some x when x = dummy_var -> rs.mutref <- None
            | _ -> ());
          match rs.status with
            | Borrowed _ ->
              if rs.refs = [] then rs.status <- Free
            | MutBorrowed _ ->
              (* Can also have const references because of if/else blocks *)
              if rs.refs = [] && rs.mutref = None then rs.status <- Free
            | _ -> ()
        ) !lrset;
      aux (if decl then lvl+1 else lvl) r in
  aux lvl (fst b.block_desc);

  (* Clean up all the variables *)
  let clean = ref true in
  while !clean do
    match RMap.max_binding_opt !lrset with
      | Some ((vname, lt), rs) when lt >= lvl ->
        RMap.iter (fun _ r ->
            if r.mutref = Some (vname, lt) then
              r.mutref <- None;
            if List.mem (vname, lt) r.refs then
              r.refs <- List.filter (fun s -> s <> (vname, lt)) r.refs;
          ) !lrset;
        lrset := RMap.remove (vname, lt) !lrset;
      | _ -> clean := false
  done


(* Entry point of this module *)
let check f =
  List.iter (fun a -> match a with
      | DeclStruct (s, fields) ->
        structs := SMap.add s fields !structs;
      | DeclFun (fname, args, _, b) ->
        lrset := RMap.empty;
        List.iter (fun a ->
            let argname, (_, _, t) = a in
            match t with
              | Struct sname | Tref (_, Struct sname)  ->
                List.iter (fun v' ->
                    let new_rs =
                      { refs = [];
                        mutref = None;
                        status = Free
                      } in
                    lrset := RMap.add (v', 0) new_rs !lrset;
                  ) (list_children argname sname !structs);
              | _ ->
                lrset := RMap.add (argname, 0)
                    { refs = [];
                      mutref = None;
                      status = Free;
                    } !lrset
          ) args;
        check_block b 0
    ) (List.rev f)
