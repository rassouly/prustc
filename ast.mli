open Lexing
open Helper

type expr = {
    expr_desc: expr_desc;
    expr_loc: loc;
}

and expr_desc =
  | Const of int
  | Bconst of bool
  | Var of string
  | Binop of binop * expr * expr
  | Unop of unop * expr
  | Dot of expr * string
  | Len of expr
  | At of expr * expr
  | FunApp of string * expr list
  | Vec of expr list
  | Print of string
  | Block of block

and selse = {
    else_desc: else_desc;
    else_loc: loc;
}

and else_desc =
  | ElseBlock of block
  | ElseIf of sif

and sif = {
    if_desc: expr * block * selse option;
    if_loc: loc;
}

and instr = {
    instr_desc: instr_desc;
    instr_loc: loc;
}

and instr_desc =
  | Empty
  | Expr of expr
  | Decl of mut * string * expr
  | StructInst of mut * string * string * (string * expr) list
  | While of expr * block
  | Return of expr option
  | If of sif

and block = {
    block_desc: instr list * expr option;
    block_loc: loc;
}

type type_annot = {
    type_annot_desc: type_annot_desc;
    type_annot_loc: loc;
}

and type_annot_desc =
  | TypeName of string
  | Template of string * type_annot
  | TypeRef of mut * type_annot

type arg = {
    arg_desc: mut * string * type_annot;
    arg_loc: loc;
}

type decl = {
    decl_desc: decl_desc;
    decl_loc: loc;
}

and decl_desc =
  | DeclStruct of string * (string * type_annot) list
  | DeclFun of string * (arg list) * type_annot option * block

type file = decl list
